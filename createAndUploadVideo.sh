#!/bin/bash
VIDEO_FILE="/home/pi/Videos/$(date +"%Y-%m-%d").mp4"
SOURCE_IMAGE_DIR="/home/pi/Pictures/webcam/$(date +"%Y-%m-%d")"

pushd $SOURCE_IMAGE_DIR
ffmpeg -framerate 25 -pattern_type glob -i '*.jpg' -c:v libx264 -vf "fps=25,format=yuv420p" $VIDEO_FILE && /home/pi/Dropbox-Uploader/dropbox_uploader.sh upload $VIDEO_FILE /
popd

/home/pi/Documents/pi-picture-uploader/notifyComplete.sh
